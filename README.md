# react-native-eat-mubarak

## Getting started

`$ npm install react-native-eat-mubarak --save`

### Mostly automatic installation

`$ react-native link react-native-eat-mubarak`

### Manual installation

#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-eat-mubarak` and add `RNEatMubarak.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNEatMubarak.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

##### Or Use CocoaPod

```ruby
pod 'RNEatMubarak', path: "../node_modules/react-native-eat-mubarak/ios"
```

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`

- Add `import com.fetchsky.RNEatMubarak.RNEatMubarakPackage;` to the imports at the top of the file
- Add `new RNEatMubarakPackage()` to the list returned by the `getPackages()` method

  2.Append the following lines to `android/settings.gradle`:

```groovy
include ':react-native-eat-mubarak'
project(':react-native-eat-mubarak').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-eat-mubarak/android')
```

3.Insert the following lines inside the dependencies block in `android/app/build.gradle`:

```groovy
compile project(':react-native-eat-mubarak')
```

## Usage

```javascript
import RNEatMubarak from "react-native-eat-mubarak";

// For Order Lists
RNEatMubarak.orderList({
  location: {
    longitude: 67.0642024,
    latitude: 24.8786511
  },
  userName: user.name,
  userPhone: user.contactNumber.startsWith("+")
    ? user.contactNumber
    : `+${user.contactNumber}`,
  userEmail: user.email || `${user.id}@example.com`,
  apiKey: configs.EAT_MUBARAK_KEY,
  staging: configs.EAT_MUBARAK_STAGING,
  colour: colors.headerBackground
})
  .then(resp => {
    Log(resp);
  })
  .catch(err => {
    /**
     * TODO:
     * Handle error here
     */
    Log("Promise error", err);
  });

// For Branch Menu
RNEatMubarak.branchMenu({
  location: {
    longitude: 67.0642024,
    latitude: 24.8786511
  },
  userName: user.name,
  userPhone: `+${user.contactNumber}`,
  userEmail: user.email || `${user.id}@example.com`,
  branchId: branchId,
  apiKey: configs.EAT_MUBARAK_KEY,
  staging: configs.EAT_MUBARAK_STAGING,
  colour: colors.headerBackground
})
  .then(resp => {
    Log(resp);
  })
  .catch(err => {
    /**
     * TODO:
     * Handle error here
     */
    Log("Promise error", err);
  });

// For Restaurant List
RNEatMubarak.restaurantList({
  location: {
    longitude: 67.0642024,
    latitude: 24.8786511
  },
  userName: user.name,
  userPhone: `+${user.contactNumber}`,
  userEmail: user.email || `${user.id}@example.com`,
  apiKey: configs.EAT_MUBARAK_KEY,
  staging: configs.EAT_MUBARAK_STAGING,
  colour: colors.headerBackground
})
  .then(resp => {
    Log(resp);
  })
  .catch(err => {
    /**
     * TODO:
     * Handle error here
     */
    Log("Promise error", err);
  });
```
