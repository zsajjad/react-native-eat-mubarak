//
//  RNEatMubarakViewController.h
//  RNEatMubarak
//
//  Created by Zain Sajjad on 19/11/2018.
//

#import <UIKit/UIKit.h>
#import <React/RCTBridge.h>
#import <EatMubarakFramework/EatMubarakFramework.h>

@interface RNEatMubarakViewController : UIViewController

//@property(nonatomic, strong) EMMasterController *instance;
- (instancetype) init;

- (IBAction)branchMenu: (NSDictionary*)params
              resolver: (RCTPromiseResolveBlock)resolve
              rejecter: (RCTPromiseRejectBlock)reject;

- (IBAction)orderList: (NSDictionary*)params
              resolver: (RCTPromiseResolveBlock)resolve
              rejecter: (RCTPromiseRejectBlock)reject;
@end
