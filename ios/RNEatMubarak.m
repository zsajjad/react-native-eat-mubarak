
#import "RNEatMubarak.h"
#import "RNEatMubarakViewController.h"

@implementation RNEatMubarak

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}
RCT_EXPORT_MODULE()

- (NSDictionary *)extractBasicParams:(NSDictionary *)data {
    NSDictionary *params = @{
                             @"userPhone": [data valueForKey:@"userPhone"],
                             @"userEmail": [data valueForKey:@"userEmail"],
                             @"userName": [data valueForKey:@"userName"],
                             @"apiKey": [data valueForKey:@"apiKey"],
                             @"staging": [data valueForKey:@"staging"],
                             @"colour": [data valueForKey:@"colour"],
                             };
    return params;
}

RCT_REMAP_METHOD(branchMenu, branchMenu:(NSDictionary *)data
                  resolver: (RCTPromiseResolveBlock)resolve
                  rejecter: (RCTPromiseRejectBlock)reject)
{
    @try {
        NSDictionary *params = @{
                                 @"branchId": [data valueForKey:@"branchId"],
                                 @"googleMapsApiKey": [data valueForKey:@"googleMapsApiKey"],
                                 @"lat": [[data objectForKey:@"location"] valueForKey:@"latitude"],
                                 @"lng": [[data objectForKey:@"location"] valueForKey:@"longitude"],
                                 };
        NSMutableDictionary *finalParams = [NSMutableDictionary new];
        [finalParams addEntriesFromDictionary:[self extractBasicParams:data]];
        [finalParams addEntriesFromDictionary:params];
        
        RNEatMubarakViewController *vc = [[RNEatMubarakViewController alloc] init];
        [vc branchMenu:finalParams resolver:resolve rejecter:reject];
    }
    @catch (NSException * e) {
        reject(@"EM Failed", @"Could not start", [self errorFromException:e]);
    }
}


RCT_REMAP_METHOD(orderList, orderList:(NSDictionary *)data
                  resolver: (RCTPromiseResolveBlock)resolve
                  rejecter: (RCTPromiseRejectBlock)reject)
{
    @try {
        NSMutableDictionary *params = [NSMutableDictionary new];
        [params addEntriesFromDictionary:[self extractBasicParams:data]];

        RNEatMubarakViewController *vc = [[RNEatMubarakViewController alloc] init];
        [vc orderList:params resolver:resolve rejecter:reject];
    }
    @catch (NSException * e) {
        reject(@"EM Failed", @"Could not start", [self errorFromException:e]);
    }
}


- (NSError *) errorFromException: (NSException *) exception
{
    NSDictionary *exceptionInfo = @{
                                    @"name": exception.name,
                                    @"reason": exception.reason,
                                    @"callStackReturnAddresses": exception.callStackReturnAddresses,
                                    @"callStackSymbols": exception.callStackSymbols,
                                    @"userInfo": exception.userInfo
                                    };
    
    return [[NSError alloc] initWithDomain: @"RNEatMubarak"
                                      code: 0
                                  userInfo: exceptionInfo];
}

@end
  
