//
//  RNEatMubarakViewController.m
//  RNEatMubarak
//
//  Created by Zain Sajjad on 19/11/2018.
//
#import "RNEatMubarakViewController.h"
#import <React/RCTUtils.h>
#import <React/RCTLog.h>
#import <EatMubarakFramework/EatMubarakFramework.h>

@implementation RNEatMubarakViewController
{
    EMMasterController *_emMaster;
    RCTPromiseResolveBlock _resolve;
    RCTPromiseRejectBlock _reject;
    bool initialized;
    bool isRunning;
};

- (instancetype) init {
    self = [super init];
    _emMaster = [[EMMasterController alloc] init];
    _emMaster.parent = self;
    initialized = true;
    return self;
}

- (void)branchMenu: (NSDictionary *)params
              resolver: (RCTPromiseResolveBlock)resolve
              rejecter: (RCTPromiseRejectBlock)reject;
{
    if (!initialized) {
        return;
    }
    _resolve = resolve;
    _reject = reject;
    [self->_emMaster initializeWithRestaurantMenuWithBranchID:[params valueForKey:@"branchId"]
                                                       andLat:[[params objectForKey:@"lat"] doubleValue]
                                                       andLng:[[params objectForKey:@"lng"] doubleValue]
                                                 andUserPhone:[params valueForKey:@"userPhone"]
                                                  andUserName:[params valueForKey:@"userName"]
                                                 andUserEmail:[params valueForKey:@"userEmail"]
                                                    andAPIKey:[params valueForKey:@"apiKey"]
                                          andGoogleMapsApiKey:[params valueForKey:@"googleMapsApiKey"]
                                       andNavigationBarColour:[params valueForKey:@"colour"]
                                                   andStaging:[[params valueForKey:@"staging"] boolValue]];
    UIViewController *rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
    [rootViewController presentViewController:self animated:YES completion:NULL];
}


- (void)orderList: (NSDictionary *)params
          resolver: (RCTPromiseResolveBlock)resolve
          rejecter: (RCTPromiseRejectBlock)reject;
{
    if (!initialized) {
        return;
    }
    [self->_emMaster showUserOrdersListForUserWithPhone:[params valueForKey:@"userPhone"]
                                            andUserName:[params valueForKey:@"userName"]
                                           andUserEmail:[params valueForKey:@"userEmail"]
                                              andAPIKey:[params valueForKey:@"apiKey"]
                                 andNavigationBarColour:[params valueForKey:@"colour"]
                                             andStaging:[[params valueForKey:@"staging"] boolValue]];
    UIViewController *rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
    [rootViewController presentViewController:self animated:YES completion:NULL];
    resolve(@YES);
}

- (void)orderPlacedWithOrderId:(NSString*)orderId
                andOrderAmount:(NSString*)amount
             andRestaurantName:(NSString*)name
            andDeliveryAddress:(NSString*)address
       andDeliveryEtaInMinutes:(NSString*)eta; {
    if (_resolve) {
        NSDictionary *output = @{
                                @"orderId": orderId,
                                @"orderAmount": amount,
                                @"restaurantName": name,
                                @"deliveryAddress": address,
                                @"deliveryEta": eta,
                                };
        _resolve(output);
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    if (isRunning) {
        [self dismissViewControllerAnimated:YES completion:nil];
        isRunning = false;
        return;
    }
    isRunning = true;
}

- (void)viewWillDisappear:(BOOL)animated {
}

- (void)viewControllerDidCancel:(UIViewController*)vc
{
}

@end

