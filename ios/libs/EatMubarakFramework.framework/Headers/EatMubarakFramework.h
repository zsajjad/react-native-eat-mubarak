//
//  EatMubarakFramework.h
//  EatMubarakFramework
//
//  Created by Hyder Abbas on 10/07/2018.
//  Copyright © 2018 Hyder Abbas. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for EatMubarakFramework.
FOUNDATION_EXPORT double EatMubarakFrameworkVersionNumber;

//! Project version string for EatMubarakFramework.
FOUNDATION_EXPORT const unsigned char EatMubarakFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <EatMubarakFramework/PublicHeader.h>

#import <EatMubarakFramework/EMMasterController.h>
