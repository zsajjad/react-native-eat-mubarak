//
//  MasterController.h
//  EatMubarakFramework
//
//  Created by Hyder Abbas on 10/07/2018.
//  Copyright © 2018 Hyder Abbas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface EMMasterController : NSObject
@property (nonatomic,strong) UIViewController *parent;

- (void)initializeWithRestaurantForLocationWithLat:(double)lat andLng:(double)lng andUserPhone:(NSString*)phone andUserName:(NSString*)name andUserEmail:(NSString*)email andAPIKey:(NSString*)key andGoogleMapsApiKey:(NSString*)gApiKey andNavigationBarColour:(NSString*)colourHex andStaging:(BOOL)staging;

- (void)showUserOrdersListForUserWithPhone:(NSString*)phone andUserName:(NSString*)name andUserEmail:(NSString*)email andAPIKey:(NSString*)key andNavigationBarColour:(NSString*)colourHex andStaging:(BOOL)staging;

- (void)initializeWithRestaurantMenuWithBranchID:(NSString*)branchID andLat:(double)lat andLng:(double)lng andUserPhone:(NSString*)phone andUserName:(NSString*)name andUserEmail:(NSString*)email andAPIKey:(NSString*)key andGoogleMapsApiKey:(NSString*)gApiKey andNavigationBarColour:(NSString*)colourHex andStaging:(BOOL)staging;

@end
