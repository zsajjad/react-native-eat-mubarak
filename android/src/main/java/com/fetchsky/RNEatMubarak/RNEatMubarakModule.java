
package com.fetchsky.RNEatMubarak;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;

import com.facebook.react.bridge.WritableMap;
import com.techworks.eatmubaraklibrary.EatMubarakWidget;
import com.techworks.eatmubaraklibrary.UserInfo;

//import java.io.IOException;

public class RNEatMubarakModule extends ReactContextBaseJavaModule {

    private ReactApplicationContext reactContext;
    private Promise pendingPromise;

    public static int APP_REQUEST_CODE = 72;

    private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {
        @Override
        public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
            if (requestCode == APP_REQUEST_CODE) {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    WritableMap map = Arguments.createMap();
                    try {
                        map.putString("orderId", data.getStringExtra("orderId"));
                        map.putString("orderAmount", data.getStringExtra("orderAmount"));
                        map.putString("restaurantName", data.getStringExtra("restaurantName"));
                        map.putString("deliveryAddress", data.getStringExtra("deliveryAddress"));
                        map.putString("deliveryEta", data.getStringExtra("deliveryEta"));
                        map.putString("isCod", data.getStringExtra("isCod"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("EM", e.getMessage());
                    } finally {
                        resolvePromise(map);
                    }
                } else {
                    rejectPromise("error", new Error("Eat Mubarak failed"));
                }
            }
        }
    };

    public RNEatMubarakModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        reactContext.addActivityEventListener(mActivityEventListener);
    }

    private EatMubarakWidget.Builder getEMIntent(ReadableMap options) {
        UserInfo userInfo = new UserInfo();
        userInfo.setUserContact(options.getString("userPhone")); // User's Phone Number
        userInfo.setUserEmail(options.getString("userEmail")); // User's Email Address
        userInfo.setUserName(options.getString("userName")); // User's Name
        return EatMubarakWidget.Builder.getBuilder()
                .setUserInfo(userInfo) // User Information Object
                .setLocation(options.getMap("location").getDouble("longitude"), options.getMap("location").getDouble("latitude")) //SetLocation(Longitude,Latitude)
                .setTimeOut(180)
                .setApiKey(options.getString("apiKey"))
                .setStaging(options.getBoolean("staging")); // Staging By Default;
    };

    @ReactMethod
    public void restaurantList(ReadableMap options, Promise promise) {
        try {
            Intent intent = getEMIntent(options)
                .build(this.reactContext.getApplicationContext());
            startActivity(intent, promise);
        } catch (Exception e) {
            rejectPromiseOnException(e);
        }
    }

    @ReactMethod
    public void orderList(ReadableMap options, Promise promise) {
        try {
            Intent intent = getEMIntent(options)
                    .getOrderList(true)
                    .build(this.reactContext.getApplicationContext());
            startActivity(intent, promise);
        } catch (Exception e) {
            rejectPromiseOnException(e);
        }
    }

    @ReactMethod
    public void branchMenu(ReadableMap options, Promise promise) {
        try {
            Intent intent = getEMIntent(options)
                    .setBranchId(options.getInt("branchId"))
                    .build(this.reactContext.getApplicationContext());
            startActivity(intent, promise);
        } catch (Exception e) {
            rejectPromiseOnException(e);
        }
    }

    public void startActivity(Intent intent, Promise promise) {
        this.reactContext.startActivityForResult(intent, APP_REQUEST_CODE, new Bundle());
        this.pendingPromise = promise;
    }

    private void rejectPromiseOnException(Exception e) {
        e.printStackTrace();
        rejectPromise("error", new Error(e.getMessage()));
    }

    private void rejectPromise(String code, Error err) {
        if (this.pendingPromise != null) {
            this.pendingPromise.reject(code, err);
            this.pendingPromise = null;
        }
    }

    private void resolvePromise(WritableMap data) {
        if (this.pendingPromise != null) {
            this.pendingPromise.resolve(data);
            this.pendingPromise = null;
        }
    }

    @Override
    public String getName() {
        return "RNEatMubarak";
    }
}
